﻿using Quartz;
using System.Threading.Tasks;

namespace H2O.Assistant.Jobs
{
    /// <summary>
    /// 用来测试下 NLog 在 Quartz's Job 中能否正常写入日志到文件
    /// 结果: 
    /// 1. ConsoleLifetime 环境下正常.
    /// 2. WindowsService 环境下正常.
    /// </summary>
    public class NLogJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            NLog.ILogger logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Trace(message: "NLog.Trace()...");
            logger.Debug(message: "NLog.Debug()...");
            logger.Info(message: "NLog.Info()...");
            logger.Warn(message: "NLog.Warn()...");
            logger.Fatal(message: "NLog.Fatal()...");
            return Task.FromResult(result: true);
        }
    }
}
