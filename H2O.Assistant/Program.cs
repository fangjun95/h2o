﻿using H2O.Assistant.HostedServices;
using H2O.Assistant.Jobs;
using H2O.Assistant.QuartzExtensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog.Extensions.Logging;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace H2O.Assistant
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .UseSystemd()
                .ConfigureServices((hostContext, services) =>
                {
                    if (hostContext.HostingEnvironment.IsDevelopment())
                    {
                        // Development service configuration
                    }
                    else
                    {
                        // Non-development service configuration
                    }

                    // Logging
                    services.AddLogging(options =>
                    {
                        options.AddNLog();
                    });

                    services.AddHttpClient();
                    //services.AddHostedService<LifetimeEventsHostedService>();
                    //services.AddHostedService<TimedHostedService>();
                    //services.AddHostedService<Worker>();

                    // Quartz
                    services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
                    services.AddSingleton<IJobFactory, CustomJobFactory>();
                    services.AddHostedService<QuartzHostedService>();
                    // Quartz Jobs
                    services.AddTransient<HelloJob>();
                    services.AddTransient<DingTalkJob>();
                    // 配置其他服务...
                });
    }
}
